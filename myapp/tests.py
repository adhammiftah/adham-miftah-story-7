from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home, confirmation
from .models import AddStatus
from .forms import AddStatus_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Story7UnitTest(TestCase):

	def test_story_7_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_7_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_model_can_create_new_status(self):
	    # Creating a new activity
	    new_activity = AddStatus.objects.create(name='test', status='ppw story add status test')
	
	    # Retrieving all available activity
	    counting_all_available_status = AddStatus.objects.all().count()
	    self.assertEqual(counting_all_available_status, 1)
	
	def test_story_7_confirmation_page_url_exist(self):
		response = Client().get('/confirmation_page/')
		self.assertEqual(response.status_code, 200)

	def test_story_7_confirmation_page_function_used(self):
		found = resolve('/confirmation_page/')
		self.assertEqual(found.func, confirmation)

class Story7FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_new_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		name = selenium.find_element_by_name('name')
		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		user_name = "User1"

		name.send_keys(user_name)
		status.send_keys('Hello there guys')
		submit.send_keys(Keys.RETURN)

		time.sleep(5)

		submit2 = selenium.find_element_by_id('submit2')
		submit2.send_keys(Keys.RETURN)

		time.sleep(5)
		page = selenium.page_source
		self.assertIn(user_name, page)

	def test_input_new_status_but_cancelled(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		name = selenium.find_element_by_name('name')
		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		user_name = "User2"

		name.send_keys(user_name)
		status.send_keys('Hello there guys')
		submit.send_keys(Keys.RETURN)

		time.sleep(5)

		cancel = selenium.find_element_by_id('cancel')
		cancel.send_keys(Keys.RETURN)

		time.sleep(5)
		page = selenium.page_source
		self.assertNotIn(user_name, page)