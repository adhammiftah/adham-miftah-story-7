from django.forms import ModelForm
from .models import AddStatus

class AddStatus_Form(ModelForm):
    class Meta:
        model = AddStatus
        fields = '__all__'