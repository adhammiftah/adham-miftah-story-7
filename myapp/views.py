from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import AddStatus_Form
from .models import AddStatus

# Create your views here

form_data = [{}]

def home(request):
	if request.method == 'POST':
		form = AddStatus_Form(request.POST)
		form_data[0]["name"] = request.POST.get("name")
		form_data[0]["status"] = request.POST.get("status")

		if form.is_valid():
			return redirect("/confirmation_page/")
		else:
			return render(request, 'landing.html', {"datas":AddStatus.objects.all().order_by('-created_date')})

	else:
			return render(request, 'landing.html', {"datas":AddStatus.objects.all().order_by('-created_date')})
			
def confirmation(request):
	if request.method == 'POST':
		form = AddStatus_Form(request.POST)
		form.save()
		return redirect("/")
	else:
		return render(request, 'confirm.html', {"form_data" : form_data})

def cancel(request):
	form_data = [{}]
	return home(request)