from django.db import models

# Create your models here.

class AddStatus(models.Model):
	name = models.CharField(max_length=100)
	status = models.CharField(max_length=27)
	created_date = models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return '%s' % self.name